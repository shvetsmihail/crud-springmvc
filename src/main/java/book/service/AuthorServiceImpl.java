package book.service;

import book.domain.Author;
import book.repo.AuthorRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AuthorServiceImpl implements AuthorService{
    @Autowired
    private AuthorRepo authorRepo;

    private Logger logger = Logger.getLogger(AuthorServiceImpl.class);

    @Override
    public List<Author> getAll() {
        logger.info("getting all authors");
        return (List<Author>) authorRepo.findAll();
    }

    @Override
    public Author getAuthorByName(String name) {
        if (name == null) {
            logger.error("name is null");
            return null;
        }
        Author author = authorRepo.findByName(name);
        if (author == null) {
            logger.info("author not found");
        }
        return author;
    }

    @Override
    public void save(Author author) {
        logger.info("author was saved");
        authorRepo.save(author);
    }

    @Override
    public void delete(Author author) {
        logger.info("author was deleted");
        authorRepo.delete(author);
    }
}
