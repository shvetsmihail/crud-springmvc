package book.controlles;

import book.domain.Author;
import book.domain.Book;
import book.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class CrudController {
    @Autowired
    private AuthorService authorService;

    @RequestMapping("/")
    public String startPage(ModelMap modelMap) {
        List<Author> authors = authorService.getAll();
        modelMap.addAttribute("authors", authors);
        return "index";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@RequestParam("author") String authorName,
                       @RequestParam("book") String bookName) {
        Book book = new Book();
        book.setName(bookName);
        Author author = authorService.getAuthorByName(authorName);
        if (author == null) {
            author = new Author();

            List<Book> books = new ArrayList<>();
            books.add(book);

            author.setName(authorName);
            author.setBooks(books);
            book.setAuthor(author);

            authorService.save(author);
            return "redirect:/";
        }

        book.setAuthor(author);
        author.getBooks().add(book);
        authorService.save(author);
        return "redirect:/";
    }

    @RequestMapping(value = "/deleteAuthor", method = RequestMethod.POST)
    public String save(@RequestParam("author") String authorName) {
        Author author = authorService.getAuthorByName(authorName);
        if (author == null) {
            return "redirect:/";
        }
        authorService.delete(author);
        return "redirect:/";
    }
}
